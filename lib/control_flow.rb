# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  ("a".."z").to_a.each { |letter| str.delete!(letter) }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  if len % 2 == 0
    str[len/2 - 1] + str[len/2]
  else
    str[len/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars.each { |char| count += 1 if VOWELS.include?(char) }
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each { |n| result *= n }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr[0...-1].each { |el| result += (el + separator) }
  if result != ""
    result += arr[-1]
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_index do |i|
    if i % 2 == 0
      str[i] = str[i].downcase
    else
      str[i] = str[i].upcase
    end
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_list = str.split
  word_list.each_with_index do |word, i|
    if word.length >= 5
      word_list[i] = word_list[i].reverse
    end
  end
  word_list.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  arr.each_with_index do |el, i|
    if el % 5 == 0 && el % 3 == 0
      arr[i] = "fizzbuzz"
    elsif el % 3 == 0
      arr[i] = "fizz"
    elsif el % 5 == 0
      arr[i] = "buzz"
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2...num).to_a.each { |el| return false if num % el == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).to_a.select { |el| num % el == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |el| prime?(el) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |el|
    if el % 2 == 0
      evens << el
    else
      odds << el
    end
  end
  if evens.length < odds.length
    evens[0]
  else
    odds[0]
  end    
end
